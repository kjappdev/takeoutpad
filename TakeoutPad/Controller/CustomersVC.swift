//
//  CustomersVC.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 2/15/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import UIKit
import CoreData

class CustomersVC: UITableViewController, NSFetchedResultsControllerDelegate {
    
    
    @IBOutlet var noOrderView: UIView!
    @IBOutlet var deletedAll: UIView!
    @IBOutlet weak var deletedAllLabel: UILabel!
    
    var controller: NSFetchedResultsController<Customer>!
    var restaurant: Restaurant!
    var showOrder: Bool = false
    var addOrder: Bool = false
    var editOptionsBtn: UIBarButtonItem!
    var doneBtn: UIBarButtonItem!
    var noOrdersMsg: String = "Tap the edit icon above to add an order"
    var ordersDeletedMsg: String = "All orders for this restaurant have been deleted"
    var numRows: Int!
    var numImages: Int = 0
    var hasOrders: Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        attemptFetch()
        setBackground()
        
        self.navigationItem.title = restaurant?.name
        
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        }
        
        //set navigation bar buttons
        let editIcon = UIImage(named: "edit-icon")
        editOptionsBtn = UIBarButtonItem(image: editIcon, style: .plain, target: self, action: #selector(showEditOptions(_:)))
        doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.endTableEditMode))// create the done button
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0.5))
        footerView.backgroundColor = UIColor(red: 11/255.0, green: 72/255.0, blue: 37/255.0, alpha: 1.0)
        tableView.tableFooterView = footerView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackground()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return controller.sections?.count ?? 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = controller.sections, sections.count > 0 {
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 69
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerCell", for: indexPath) as! CustomerCell
        configureCell(cell: cell, indexPath: indexPath as NSIndexPath)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let objs = controller.fetchedObjects, objs.count > 0 {
            
            let customer = objs[indexPath.row]
            performSegue(withIdentifier: "showOrder", sender: customer)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let customers = controller.fetchedObjects, customers.count > 0 {
                let customer = customers[indexPath.row]
                context.delete(customer)
                ad.saveContext()
            }
            self.setDeletedBackground(msg: self.ordersDeletedMsg)
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            if let restaurants = self.controller.fetchedObjects, restaurants.count > 0 {
                let restaurant = restaurants[indexPath.row]
                context.delete(restaurant)
                ad.saveContext()
            }
            self.setDeletedBackground(msg: self.ordersDeletedMsg)
        }
        
        return [delete]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showOrder" {
            if let destination = segue.destination as? OrderVC {
                if let customer = sender as? Customer {
                    showOrder = true
                    destination.isFromSearch = false
                    destination.restaurant = restaurant
                    destination.navigationItem.title = restaurant.name
                    destination.customerToEdit = customer
                    destination.isShowOrder = showOrder
                }
            }
        } else if segue.identifier == "addOrder" {
            if let navVC = segue.destination as? UINavigationController {
                if let destination = navVC.topViewController as? OrderVC {
                    addOrder = true
                    destination.isFromSearch = false
                    destination.restaurant = restaurant
                    destination.navigationItem.title = restaurant.name
                    destination.isAddOrder = addOrder
                }
            }
        }
        
    }
    
    
    func configureCell(cell: CustomerCell, indexPath: NSIndexPath) {
        let customer = controller.object(at: indexPath as IndexPath)
        cell.configureCell(customer: customer)
    }
    
    func attemptFetch() {
        let fetchRequest: NSFetchRequest<Customer> = Customer.fetchRequest()
        let titleSort = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare))
        let predicate1 = NSPredicate(format: "whichRestaurant.name == %@", (self.restaurant.name)!)
        //print(self.restaurant.name!)
        //print(self.restaurant.unique)
        let predicate2 = NSPredicate(format: "whichRestaurant.unique == %i", self.restaurant.unique)
        fetchRequest.sortDescriptors = [titleSort]
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate1, predicate2])
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        controller.delegate = self
        
        self.controller = controller
        
        do {
            try controller.performFetch()
        } catch {
            let error = error as NSError
            print("\(error)")
        }
        //tableView.reloadData()
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
            
        case.insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .bottom)
            }
            break
        case.delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .bottom)
            }
            break
        case.update:
            if let indexPath = indexPath {
                let cell = tableView.cellForRow(at: indexPath) as! CustomerCell
                configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
            }
            break
        case.move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .bottom)
            }
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .bottom)
            }
            break
        }
    }
    
    func generateTestData() {
        
        let customer1 = Customer(context: context)
        customer1.name = "David Jones"
        customer1.whichRestaurant = self.restaurant
        
        let customer2 = Customer(context: context)
        customer2.name = "Armenia"
        customer2.whichRestaurant = self.restaurant
        
        let customer3 = Customer(context: context)
        customer3.name = "Kevin Jefferson"
        customer3.whichRestaurant = self.restaurant
        
        ad.saveContext()
        
    }
    
    @IBAction func showEditOptions(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let addOrderAction = UIAlertAction(title: "Add a New Order", style: .default) { action in
            self.performSegue(withIdentifier: "addOrder", sender: self)
        }
        
        let deleteOrderAction = UIAlertAction(title: "Delete an Order", style: .destructive) { action in
            self.setEditing(true, animated: true)
            self.navigationItem.rightBarButtonItem = self.doneBtn  // assign button
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            // handle cancel response here. Doing nothing will dismiss the view.
        }
        
        alert.addAction(addOrderAction)
        if self.hasOrders {
            alert.addAction(deleteOrderAction)
        }
        alert.addAction(cancelAction)
        
        alert.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem //For iPad popover issue
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func endTableEditMode(_ sender: UIBarButtonItem) {
        self.setEditing(false, animated: true)
        setBackground()
        self.navigationItem.rightBarButtonItem = editOptionsBtn //restore the compose button after editing
    }
    
    func setBackground() {
        if let objs = controller.fetchedObjects, objs.count > 0 {
            self.tableView.backgroundView = nil
            self.hasOrders = true
        } else {
            self.tableView.backgroundView = noOrderView
            self.hasOrders = false
        }
    }
    
    func setDeletedBackground(msg: String) {
        self.deletedAllLabel.text = ordersDeletedMsg
        if let objs = controller.fetchedObjects, objs.count > 0 {
            self.tableView.backgroundView = nil
            self.hasOrders = true
        } else {
            self.tableView.backgroundView = deletedAll
            self.hasOrders = false
        }
    }
    
    
}
