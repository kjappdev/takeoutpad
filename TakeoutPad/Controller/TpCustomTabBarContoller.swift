//
//  TpCustomTabBarContoller.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 3/8/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import UIKit

class TpCustomTabBarContoller: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.tintColor = UIColor(red: 17/255.0, green: 130/255.0, blue: 94/255.0, alpha: 1.0)
        self.tabBar.backgroundColor = UIColor.white
        self.tabBar.items![0].title = "Home"
        self.tabBar.items![0].image = UIImage(named: "home_unselected")
        self.tabBar.items![0].selectedImage = UIImage(named: "home_selected")
        self.tabBar.items![1].title = "Search"
        self.tabBar.items![1].image = UIImage(named: "search_unselected")
        self.tabBar.items![1].selectedImage = UIImage(named: "search_selected")
        
        
        
        let attrsNormal = [
            NSAttributedStringKey.foregroundColor: UIColor.lightGray
        ]
        let attrsSelected = [
            NSAttributedStringKey.foregroundColor: UIColor(red: 17/255.0, green: 130/255.0, blue: 94/255.0, alpha: 1.0)
        ]
        UITabBarItem.appearance().setTitleTextAttributes(attrsNormal, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(attrsSelected, for: .selected)
        
    }
}
