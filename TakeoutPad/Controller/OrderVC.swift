//
//  AddOrderVC.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 2/25/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import UIKit
import MessageUI
import AVFoundation

class OrderVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet weak var orderNameField: UITextField!
    @IBOutlet weak var orderDetailsField: UITextView!
    @IBOutlet weak var detailsContainer: TpCustomView!
    @IBOutlet weak var scrollContainer: UIScrollView!
    @IBOutlet weak var customerImage: UIImageView!
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var imageBtn: UIButton!
    @IBOutlet weak var shareBtn: UIBarButtonItem!
    
    
    
    var editBtn: UIBarButtonItem!
    var doneBtn: UIBarButtonItem!
    var cancelBtn: UIBarButtonItem!
    var backBtn: UIBarButtonItem!
    var saveBtn: UIBarButtonItem!
    var cancelSaveBtn: UIBarButtonItem!
    var leftBarBtn: UIBarButtonItem!
    var rightBarBtn: UIBarButtonItem!
    var isFromSearch: Bool!
    var isShowOrder: Bool!
    var isAddOrder: Bool!
    var activeTextField: UITextView!
    var placeholderLabel : UILabel!
    var customerToAdd: Customer!
    var imagePicker: UIImagePickerController!
    var restaurant: Restaurant!
    var customerToEdit: Customer!
    var messageDetails: String!
    
    var customProvider: CustomProvider!
    var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.center = view.center
        self.activityIndicator.color = UIColor.gray
        //set the textview and textfield delegates
        self.orderDetailsField.delegate = self
        self.orderNameField.delegate = self
        self.orderDetailsField.isScrollEnabled = false
        self.scrollContainer.delegate = self
        //self.scrollTop = CGPoint(x: 0, y: -self.scrollContainer.contentInset.top)

        // Do any additional setup after loading the view.
        //setBackground()
        setBackButton()
        
        //self.detailsContainer.backgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1.0)
        
        self.orderNameField.attributedPlaceholder = NSAttributedString(string: "Enter a name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        
        // Register to be notified if the keyboard is changing size
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShowOrHide), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShowOrHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(addCustomerImage))
        photoLabel.isUserInteractionEnabled = true
        photoLabel.addGestureRecognizer(tap)
        
        //set editing buttons
        backBtn = self.navigationItem.backBarButtonItem
        editBtn = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editOrder(_:)))
        doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(endEdit(_button:)))
        saveBtn = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveOrder(_button:)))
        cancelBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelEdit(_button:)))
        cancelSaveBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelSave(_button:)))
        
        self.setBottomBorder()
        self.customerImage.layer.cornerRadius = 5.0
        
        if isShowOrder == true {
            setShowOrderStyles()
            imageBtn.isEnabled = false
            photoLabel.isUserInteractionEnabled = false
            shareBtn.isEnabled = true
            shareBtn.image = UIImage(named: "share-icon")
            self.photoLabel.text = ""
            self.orderNameField.text = customerToEdit.name
            self.orderDetailsField.text = customerToEdit.order
            self.customerImage.image = customerToEdit.toCustomerImage?.image as? UIImage
            if let currentImage =  customerToEdit.toCustomerImage?.image as? UIImage {
                customerImage.image = currentImage
            } else {
                customerImage.image = UIImage(named: "imagePick")
            }
        }
        
        if isAddOrder == true {
            setAddOrderStyles()
            imageBtn.isEnabled = true
            photoLabel.isUserInteractionEnabled = true
            self.photoLabel.text = "Add a photo"
            self.navigationItem.rightBarButtonItem = saveBtn
            self.navigationItem.leftBarButtonItem = cancelSaveBtn
        }
        self.setPlaceHolder()
    }
    
    deinit {
        // Don't have to do this on iOS 9+, but it still works
        NotificationCenter.default.removeObserver(self)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage {
            customerImage.image = resizeImage(image: img, newWidth: 300)
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    @objc func keyboardWillShowOrHide(notification: NSNotification) {
        
        if let scrollView = scrollContainer, let userInfo = notification.userInfo, let endValue = userInfo[UIKeyboardFrameEndUserInfoKey], let durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey] {
            
            let endRect = view.convert((endValue as AnyObject).cgRectValue, from: view.window)
            
            let keyboardOverlap = scrollView.frame.maxY - endRect.origin.y
            
            // Avoid the keyboard
            scrollView.contentInset.bottom = keyboardOverlap
            scrollView.scrollIndicatorInsets.bottom = keyboardOverlap
            
            let duration = (durationValue as AnyObject).doubleValue
            UIView.animate(withDuration: duration!, delay: 0, options: .beginFromCurrentState, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        setEditNavBarBtns()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        setEditNavBarBtns()
        if isShowOrder == true {
            imageBtn.isEnabled = true
            photoLabel.isUserInteractionEnabled = true
            self.photoLabel.text = "Edit Photo"
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !orderDetailsField.text.isEmpty
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        //Layout root view and subviews
        //Takes care of cursor positon "Return key" issue
        UIView.animate(withDuration: 0.0, delay: 0, options: .beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func editOrder(_ sender: UIBarButtonItem) {
        self.photoLabel.text = "Edit Photo"
        self.navigationItem.rightBarButtonItem = doneBtn
        self.navigationItem.leftBarButtonItem = cancelBtn
        imageBtn.isEnabled = true
        photoLabel.isUserInteractionEnabled = true
        self.orderDetailsField.becomeFirstResponder()
    }
    
    @IBAction func addCustomerImage(_ sender: UIButton) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let addPhotoAction = UIAlertAction(title: "Choose a photo", style: .default) { action in
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.imagePicker = UIImagePickerController()
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: {
                    self.activityIndicator.stopAnimating()
                })
            } else {
                // show failure alert
                let photoLibraryAlert = UIAlertController(title: nil, message: "Your device is not able to access the photo library.", preferredStyle: .alert)
                
                let okPhotoLibraryAction = UIAlertAction(title: "Ok", style: .default) { action in
                    // handle response here. Doing nothing will dismiss the view.
                }
                photoLibraryAlert.addAction(okPhotoLibraryAction)
                self.present(photoLibraryAlert, animated: true, completion: nil)
                self.activityIndicator.stopAnimating()
            }
            
        }
        
        let takePhotoAction = UIAlertAction(title: "Take a photo", style: .default) { action in
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                let authStatus = AVCaptureDevice.authorizationStatus(for: .video)
                
                switch authStatus {
                case .authorized: self.presentCamera()
                case .notDetermined: self.requestCameraPermission()
                case .restricted, .denied: self.alertCameraAccessNeeded()
                }
                
            } else {
                // show failure alert
                let cameraAlert = UIAlertController(title: nil, message: "Your device is not able to access the camera.", preferredStyle: .alert)
                
                let okCameraAction = UIAlertAction(title: "Ok", style: .default) { action in
                    // handle response here. Doing nothing will dismiss the view.
                }
                cameraAlert.addAction(okCameraAction)
                self.present(cameraAlert, animated: true, completion: nil)
                self.activityIndicator.stopAnimating()
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            // handle cancel response here. Doing nothing will dismiss the view.
        }
        
        alert.addAction(addPhotoAction)
        alert.addAction(takePhotoAction)
        alert.addAction(cancelAction)
        
        self.view.endEditing(true)
        alert.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem //For iPad popover issue
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func shareOrder(_ sender: UIBarButtonItem) {
        if customerToEdit.order == "" {
            messageDetails = "\(restaurant.name!) has no order details for this customer"
        } else {
            messageDetails = customerToEdit.order
        }

        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let sendOrderEmail = UIAlertAction(title: "Send an Email", style: .default) { action in
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
            if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.navigationBar.tintColor = UIColor.white
                mail.setSubject("\(self.restaurant.name!) order for \(self.customerToEdit.name!)")
                mail.setMessageBody("Restaurant:\n\(self.restaurant.name!)\nName:\n\(self.customerToEdit.name!)\nOrder:\n\(self.messageDetails!)", isHTML: false)
                
                self.present(mail, animated: true, completion: {
                    UIApplication.shared.statusBarStyle = .lightContent
                    self.activityIndicator.stopAnimating()
                })
            } else {
                // show failure alert
                let mailErrorAlert = UIAlertController(title: nil, message: "Your device is not able to send an email.", preferredStyle: .alert)
                
                let okMailAction = UIAlertAction(title: "Ok", style: .default) { action in
                    // handle response here. Doing nothing will dismiss the view.
                }
                mailErrorAlert.addAction(okMailAction)
                self.present(mailErrorAlert, animated: true, completion: nil)
                self.activityIndicator.stopAnimating()
            }
        }
        
        let sendOrderText = UIAlertAction(title: "Send a Text", style: .default) { action in
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
            let attrs = [
                NSAttributedStringKey.foregroundColor: UIColor.white
            ]
            let navImage = UIImage.from(color: UIColor(red: 17/255.0, green: 92/255.0, blue: 51/255.0, alpha: 0.9))
            if MFMessageComposeViewController.canSendText() {
                UINavigationBar.appearance().setBackgroundImage(navImage, for: .default)
                UIBarButtonItem.appearance().setTitleTextAttributes(attrs, for: .normal)
                
                let textMsg = MFMessageComposeViewController()
                textMsg.messageComposeDelegate = self
                textMsg.body = "Restaurant:\n\(self.restaurant.name!)\nName:\n\(self.customerToEdit.name!)\nOrder:\n\(self.messageDetails!)"
                
                self.present(textMsg, animated: true, completion: {
                    UIApplication.shared.statusBarStyle = .lightContent
                    self.activityIndicator.stopAnimating()
                })
            } else {
                // show failure alert
                let textErrorAlert = UIAlertController(title: nil, message: "Your device is not able to send text messages.", preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "Ok", style: .default) { action in
                    // handle response here. Doing nothing will dismiss the view.
                }
                textErrorAlert.addAction(okAction)
                self.present(textErrorAlert, animated: true, completion: nil)
                self.activityIndicator.stopAnimating()
            
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            // handle cancel response here. Doing nothing will dismiss the view.
        }
        
        
        
        alert.addAction(sendOrderText)
        alert.addAction(sendOrderEmail)
        alert.addAction(cancelAction)
        
        alert.popoverPresentationController?.barButtonItem = self.shareBtn // For iPad popover issue
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true)
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    @objc func saveOrder(_button: UIBarButtonItem!) {
        if isValidOrder() {
            saveData()
            self.view.endEditing(true)
            self.dismiss(animated: true, completion: nil)
        } else {
            let noNameAlert = UIAlertController(title: nil, message: "Please enter a name.", preferredStyle: .alert)
            
            let okNoNameAction = UIAlertAction(title: "Ok", style: .default) { action in
                // handle response here. Doing nothing will dismiss the view.
            }
            noNameAlert.addAction(okNoNameAction)
            self.present(noNameAlert, animated: true, completion: nil)
        }
    }
    
    @objc func endEdit(_button: UIBarButtonItem!) {
        
        if isValidOrder() {
            saveData()
            self.view.endEditing(true)
            self.shareBtn.isEnabled = true
            self.shareBtn.image = UIImage(named: "share-icon")
            self.photoLabel.text = ""
            self.navigationItem.rightBarButtonItem = editBtn
            self.navigationItem.leftBarButtonItem = backBtn
            photoLabel.isUserInteractionEnabled = false
            imageBtn.isEnabled = false
        } else {
            let noNameAlert = UIAlertController(title: nil, message: "Please enter a name.", preferredStyle: .alert)
            
            let okNoNameAction = UIAlertAction(title: "Ok", style: .default) { action in
                // handle response here. Doing nothing will dismiss the view.
            }
            noNameAlert.addAction(okNoNameAction)
            self.present(noNameAlert, animated: true, completion: nil)
        }
        
    }
    
    @objc func cancelEdit(_button: UIBarButtonItem!) {
        self.orderNameField.text = customerToEdit.name
        self.orderDetailsField.text = customerToEdit.order
        self.customerImage.image = customerToEdit.toCustomerImage?.image as? UIImage
        self.view.endEditing(true)
        self.shareBtn.isEnabled = true
        self.shareBtn.image = UIImage(named: "share-icon")
        self.photoLabel.text = ""
        self.navigationItem.rightBarButtonItem = editBtn
        self.navigationItem.leftBarButtonItem = backBtn
        photoLabel.isUserInteractionEnabled = false
        imageBtn.isEnabled = false
    }
    
    @objc func cancelSave(_button: UIBarButtonItem!) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func setEditNavBarBtns() {
        self.shareBtn.isEnabled = false
        self.shareBtn.image = nil
        if isShowOrder == true {
            self.navigationItem.leftBarButtonItem = cancelBtn
            self.navigationItem.rightBarButtonItem = doneBtn
        } else if isAddOrder == true {
            self.navigationItem.leftBarButtonItem = cancelSaveBtn
            self.navigationItem.rightBarButtonItem = saveBtn
        }
    }
    
    func setBottomBorder() {
        self.orderNameField.borderStyle = .none
        self.orderNameField.layer.backgroundColor = UIColor.white.cgColor
        self.orderNameField.tintColor = UIColor(red: 17/255.0, green: 130/255.0, blue: 94/255.0, alpha: 1.0)
        self.orderNameField.layer.masksToBounds = false
        self.orderNameField.layer.shadowColor = UIColor(red: 17/255.0, green: 130/255.0, blue: 94/255.0, alpha: 1.0).cgColor
        self.orderNameField.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.orderNameField.layer.shadowOpacity = 1.0
        self.orderNameField.layer.shadowRadius = 0.0
        self.orderDetailsField.tintColor = UIColor(red: 17/255.0, green: 130/255.0, blue: 94/255.0, alpha: 1.0)
    }
    
    func setShowOrderStyles() {
        self.orderNameField.textAlignment = NSTextAlignment.center
        //self.orderNameField.isUserInteractionEnabled = false
    }
    
    func setAddOrderStyles() {
        self.orderNameField.textAlignment = NSTextAlignment.left
        self.orderNameField.isUserInteractionEnabled = true
    }
    
    func setPlaceHolder() {
        //Set textView placeholder
        placeholderLabel = UILabel()
        placeholderLabel.text = "Enter the Order Details..."
        placeholderLabel.font = UIFont.systemFont(ofSize: (orderDetailsField.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        orderDetailsField.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (orderDetailsField.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !orderDetailsField.text.isEmpty
    }
    
    func saveData() {
        var customer: Customer!
        let strOrderName = orderNameField.text
        let strOrderDetails = orderDetailsField.text
        let trimmedOrderName = strOrderName?.trimmingCharacters(in: .whitespacesAndNewlines)
        let trimmedOrderDetails = strOrderDetails?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if customerToEdit == nil {
            customer = Customer(context: context)
        } else {
            customer = customerToEdit
        }
        
        if let customerNameField = trimmedOrderName {
            customer.name = customerNameField
            customer.toOrder?.name = customerNameField
        }
        
        if let orderDetails = trimmedOrderDetails {
            customer.order = orderDetails
            customer.toOrder?.details = orderDetails
        }
        
        customer.whichRestaurant = restaurant
        
        let picture = CustomerImage(context: context)
        picture.image = customerImage.image
        
        customer.toCustomerImage = picture
        ad.saveContext()
    }
    
    func isValidOrder() -> Bool {
        let strOrderName = orderNameField.text
        let trimmedOrderName = strOrderName?.trimmingCharacters(in: .whitespacesAndNewlines)
        if (trimmedOrderName?.isEmpty)! {
            return false
        }
        return true
    }
    
    func setBackground() {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "Paper_bkg")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
    }
    
    func setBackButton() {
        if let topItem = self.navigationController?.navigationBar.topItem {
            
            if isFromSearch {
                topItem.backBarButtonItem = UIBarButtonItem(title: "Search", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            } else {
                topItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
            }
        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func presentCamera() {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .camera
        self.imagePicker.allowsEditing = false
        self.present(self.imagePicker, animated: true, completion: {
            self.activityIndicator.stopAnimating()
        })
    }
    
    func presentPhotoLibrary() {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = false
        self.present(self.imagePicker, animated: true, completion: {
            self.activityIndicator.stopAnimating()
        })
    }
    
    func requestCameraPermission() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
            self.presentCamera()
        })
    }
    
    func requestPhotoLibraryPermission() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
            self.presentPhotoLibrary()
        })
    }
    
    func alertCameraAccessNeeded() {
        let settingsAppURL = URL(string: UIApplicationOpenSettingsURLString)!
        
        let alert = UIAlertController(
            title: "Cannot access camera",
            message: "Please turn on Camera in Settings for TakeoutPad",
            preferredStyle: UIAlertControllerStyle.alert
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true, completion: {
            self.activityIndicator.stopAnimating()
        })
    }
    
    func alertPhotoLibraryAccessNeeded() {
        let settingsAppURL = URL(string: UIApplicationOpenSettingsURLString)!
        
        let alert = UIAlertController(
            title: "Cannot access photos",
            message: "Please turn on Photos in Settings for TakeoutPad",
            preferredStyle: UIAlertControllerStyle.alert
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true, completion: {
            self.activityIndicator.stopAnimating()
        })
    }
            
}

extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}
