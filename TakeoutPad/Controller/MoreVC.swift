//
//  MoreVC.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 3/28/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import UIKit
import MessageUI

class MoreVC: UITableViewController, MFMailComposeViewControllerDelegate {
    
    var shareTypes: [String] = ["support"]
    //Facebook and Twitter share api has changed. Will add in later version.
    //var shareTypes: [String] = ["support","mail", "facebook", "twitter"]

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.navigationItem.title = "TakeoutPad"
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0.5))
        footerView.backgroundColor = UIColor(red: 11/255.0, green: 72/255.0, blue: 37/255.0, alpha: 1.0)
        tableView.tableFooterView = footerView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return shareTypes.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 69
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell", for: indexPath) as! MoreCell
        configureCell(cell: cell, indexPath: indexPath as NSIndexPath)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
        let shareMode = shareTypes[indexPath.row]
        switch shareMode {
        case "support":
            supportEmail()
            break
        case "mail":
            break
        case "facebook":
            
            break
        case "twitter":
            
            break
        default:
            supportEmail()
            break
        }
            
    }
    
    func configureCell(cell: MoreCell, indexPath: NSIndexPath) {
        let shareType = shareTypes[indexPath.row]
        cell.configureCell(shareType: shareType)
    }
    
    func shareEmail() {
        let mailImage = UIImage(named: "search_selected")
        let imageData = UIImagePNGRepresentation(mailImage!)
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.navigationBar.tintColor = UIColor.white
            mail.setSubject("TakeoutPad iPhone app")
            mail.setMessageBody("Save takeout orders of family and friends with TakeoutPad. <a href='https://itunes.apple.com/us/app/takeoutpad/id866433723?mt=8'>View in App store</a>", isHTML: true)
            
            mail.addAttachmentData(imageData!, mimeType: "image/png", fileName: "search_selected")
            
            self.present(mail, animated: true, completion: {
                UIApplication.shared.statusBarStyle = .lightContent
            })
        } else {
            // show failure alert
        }
    }
    
    func supportEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.navigationBar.tintColor = UIColor.white
            mail.setToRecipients(["support@takeoutpad.com"])
            
            self.present(mail, animated: true, completion: {
                UIApplication.shared.statusBarStyle = .lightContent
            })
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

}
