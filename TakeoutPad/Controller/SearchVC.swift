//
//  SearchVC.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 3/9/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import UIKit
import CoreData

class SearchVC: UITableViewController, UISearchResultsUpdating, NSFetchedResultsControllerDelegate, UISearchBarDelegate {
    
    @IBOutlet var noResultsView: UIView!
    @IBOutlet weak var noResultsLabel: UILabel!
    
    
    var controller: NSFetchedResultsController<Customer>!
    var searchController: UISearchController!
    var hasResults: Bool!
    var noResultsMsg: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        searchController = UISearchController(searchResultsController: nil)
        self.navigationItem.title = "Search"
        searchController.searchBar.placeholder = "Restaurant or Person"
        searchController.searchBar.searchBarStyle = UISearchBarStyle.default
        searchController.searchBar.tintColor = UIColor.white
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
    
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
            if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
                textfield.tintColor = UIColor.blue
                textfield.borderStyle = .roundedRect
                textfield.backgroundColor = UIColor.white
                
                if let backgroundview = textfield.subviews.first {
                    backgroundview.backgroundColor = UIColor.white
                    backgroundview.layer.cornerRadius = 10;
                    backgroundview.clipsToBounds = true;
                }
            }
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0.5))
        footerView.backgroundColor = UIColor(red: 11/255.0, green: 72/255.0, blue: 37/255.0, alpha: 1.0)
        tableView.tableFooterView = footerView
        
        definesPresentationContext = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        let searchText = searchController.searchBar.text
        let trimmedSearchText = searchText?.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmedSearchText?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
        setBackground()
        tableView.reloadData()
    }
    
    func filterContentForSearchText(_ searchText: String) {
        let fetchRequest: NSFetchRequest<Customer> = Customer.fetchRequest()
        let titleSort = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare))
        let restuarantSort = NSSortDescriptor(key: "whichRestaurant.name", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare))
        fetchRequest.sortDescriptors = [titleSort, restuarantSort]
        fetchRequest.predicate = NSPredicate(format: "name contains[c] %@ OR whichRestaurant.name contains[c] %@", searchText, searchText)
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        controller.delegate = self
        self.controller = controller
        
        do {
            try controller.performFetch()
        } catch {
            let error = error as NSError
            print("\(error)")
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        setBackground()
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            if let sections = controller.sections {
                let sectionInfo = sections[section]
                return sectionInfo.numberOfObjects
            }
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 69
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! SearchCell
        if isFiltering() {
            configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
        } else {
            cell.isHidden = true
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let objs = controller.fetchedObjects, objs.count > 0 {
            
            let customer = objs[indexPath.row]
            performSegue(withIdentifier: "showSearchOrder", sender: customer)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSearchOrder" {
            if let destination = segue.destination as? OrderVC {
                if let customer = sender as? Customer {
                    destination.isFromSearch = true
                    destination.isShowOrder = true
                    destination.restaurant = customer.whichRestaurant
                    destination.navigationItem.title = customer.whichRestaurant?.name
                    destination.customerToEdit = customer
                }
            }
        }
    }
    
    func configureCell(cell: SearchCell, indexPath: NSIndexPath) {
        let customer = controller.object(at: indexPath as IndexPath)
        cell.configureCell(customer: customer)
    }
    
    func setBackground() {
        if let searchText = searchController.searchBar.text {
            self.noResultsLabel.text = "No matching orders were found for '\(searchText)'"
        }
        if let objs = controller.fetchedObjects, objs.count > 0 {
            self.tableView.backgroundView = nil
        } else if searchBarIsEmpty() {
            self.tableView.backgroundView = nil
        } else {
            self.tableView.backgroundView = noResultsView
        }
    }
}
