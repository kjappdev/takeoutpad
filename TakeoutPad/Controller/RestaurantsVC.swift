//
//  RestaurantsVC.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 2/13/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import UIKit
import CoreData

class RestaurantsVC: UITableViewController, NSFetchedResultsControllerDelegate {
    
    @IBOutlet var noDataView: UIView!
    @IBOutlet var deletedAll: UIView!
    @IBOutlet weak var deleteAllLabel: UILabel!
    
    var controller: NSFetchedResultsController<Restaurant>!
    var restaurantEditing: Bool!
    var editOptionsBtn: UIBarButtonItem!
    var doneBtn: UIBarButtonItem!
    var restaurantEditDoneBtn: UIBarButtonItem!
    var hasRestaurants: Bool!
    var noRestaurantssMsg: String = "Tap the edit icon above to add a restaurant"
    var restaurantsDeletedMsg: String = "All restaurants have been deleted"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.restaurantEditing = false
        //generateTestData()
        attemptFetch()
        setBackground()
        
        self.navigationItem.title = "Restaurants"
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.endTableEditMode))// create the done button
        let editIcon = UIImage(named: "edit-icon")
        editOptionsBtn = UIBarButtonItem(image: editIcon, style: .plain, target: self, action: #selector(showEditOptions(_:)))
        restaurantEditDoneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(endRestaurantEdit))
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0.5))
        footerView.backgroundColor = UIColor(red: 11/255.0, green: 72/255.0, blue: 37/255.0, alpha: 1.0)
        tableView.tableFooterView = footerView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setBackground()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        if let sections = controller.sections {
            return sections.count
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = controller.sections {
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 69
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantCell", for: indexPath) as! RestaurantCell
        configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
        
        let accessoryBtn = UIButton(frame: CGRect(x:0, y:0, width: 0.0, height: cell.bounds.size.height * 0.5))
        accessoryBtn.setTitle("Edit", for: .normal)
        accessoryBtn.layer.cornerRadius = 5
        accessoryBtn.tintColor = UIColor.white
        accessoryBtn.backgroundColor = UIColor.blue
        accessoryBtn.addTarget(self, action: #selector(editRestaurant(_:)), for: UIControlEvents.touchUpInside)
        accessoryBtn.tag = indexPath.row
        
        if restaurantEditing {
            cell.selectionStyle = .none
            cell.accessoryView = accessoryBtn
            UIView.animate(withDuration: 0.3) {
                cell.accessoryView?.bounds.size.width = cell.bounds.size.width * 0.2
                cell.accessoryView?.layoutIfNeeded()
            }
        } else if !restaurantEditing {
            cell.selectionStyle = .default
            UIView.animate(withDuration: 0.3, animations: {
                cell.accessoryView?.bounds.size.width = 0.0
                cell.accessoryView?.layoutIfNeeded()
                
            }, completion: {(finished: Bool) in
                cell.accessoryView = nil
                })
        }

        return cell
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        if restaurantEditing {
            tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !restaurantEditing {
            if let objs = controller.fetchedObjects, objs.count > 0 {
                
                let restaurant = objs[indexPath.row]
                performSegue(withIdentifier: "showCustomers", sender: restaurant)
            }
            
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let restaurants = controller.fetchedObjects, restaurants.count > 0 {
                let restaurant = restaurants[indexPath.row]
                context.delete(restaurant)
                ad.saveContext()
            }
            self.setDeletedBackground(msg: self.restaurantsDeletedMsg)
        }
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if restaurantEditing {
            return .none
        } else {
            return .delete
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            if let restaurants = self.controller.fetchedObjects, restaurants.count > 0 {
                let restaurant = restaurants[indexPath.row]
                context.delete(restaurant)
                ad.saveContext()
            }
            self.setDeletedBackground(msg: self.restaurantsDeletedMsg)
        }
        
        return [delete]
    }
    
    override func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        if restaurantEditing {
            return false
        } else {
            return true
        }
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCustomers" {
            if let destination = segue.destination as? CustomersVC {
                if let restaurant = sender as? Restaurant {
                    destination.navigationItem.title = restaurant.name
                    destination.restaurant = restaurant
                }
            }
        } else if segue.identifier == "Add Restaurant" {
            if let navVC = segue.destination as? UINavigationController {
                if let destination = navVC.topViewController as? AddRestaurantVC {
                    if restaurantEditing {
                        if let restaurant = sender as? Restaurant {
                            destination.navigationItem.title = restaurant.name
                            destination.restaurantToEdit = restaurant
                            destination.restaurantEditMode = self.restaurantEditing
                        }
                    } else {
                        destination.navigationItem.title = "New Restaurant"
                        destination.restaurantEditMode = self.restaurantEditing
                    }
                }
            }
        }
    }
    
    
    func configureCell(cell: RestaurantCell, indexPath: NSIndexPath) {
        let restaurant = controller.object(at: indexPath as IndexPath)
        let editMode = restaurantEditing
        cell.configureCell(restaurant: restaurant, editMode: editMode)
    }
    
    func attemptFetch() {
        let fetchRequest: NSFetchRequest<Restaurant> = Restaurant.fetchRequest()
        let titleSort = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare))
        fetchRequest.sortDescriptors = [titleSort]

        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        controller.delegate = self
        
        self.controller = controller
        
        do {
            try controller.performFetch()
        } catch {
            let error = error as NSError
            print("\(error)")
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
            
        case.insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .bottom)
            }
            break
        case.delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .bottom)
            }
            break
        case.update:
            if let indexPath = indexPath {
                let cell = tableView.cellForRow(at: indexPath) as! RestaurantCell
                configureCell(cell: cell, indexPath: indexPath as NSIndexPath)
            }
            break
        case.move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .bottom)
            }
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .bottom)
            }
            break
        }
    }
    
    func generateTestData() {
        //let myImageArray = ["bbq2", "hamburger4", "chinese2", "sandwich1"]
        
        let restaurant1 = Restaurant(context: context)
        restaurant1.name = "Angus BBQ"
        let restaurant1Img = RestaurantImage(context: context)
        restaurant1Img.image = UIImage(named: "bbq2")
        restaurant1.toRestaurantImage = restaurant1Img
        
        let restaurant2 = Restaurant(context: context)
        restaurant2.name = "Big Burgers"
        let restaurant2Img = RestaurantImage(context: context)
        restaurant2Img.image = UIImage(named: "hamburger4")
        restaurant2.toRestaurantImage = restaurant2Img
        
        let restaurant3 = Restaurant(context: context)
        restaurant3.name = "China Station"
        let restaurant3Img = RestaurantImage(context: context)
        restaurant3Img.image = UIImage(named: "chinese2")
        restaurant3.toRestaurantImage = restaurant3Img
        
        let restaurant4 = Restaurant(context: context)
        restaurant4.name = "Main Street Deli"
        let restaurant4Img = RestaurantImage(context: context)
        restaurant4Img.image = UIImage(named: "sandwich1")
        restaurant4.toRestaurantImage = restaurant4Img
        
        ad.saveContext()
        
    }
    
    @IBAction func showEditOptions(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let addRestaurantAction = UIAlertAction(title: "Add a New Restaurant", style: .default) { action in
            self.performSegue(withIdentifier: "Add Restaurant", sender: self)
        }
        
        let editRestaurantAction = UIAlertAction(title: "Edit a Restaurant", style: .default) { action in
            self.restaurantEditing = true
            self.navigationItem.rightBarButtonItem = self.restaurantEditDoneBtn
            self.tableView.reloadData()
        }
        
        let deleteRestaurantAction = UIAlertAction(title: "Delete a Restaurant", style: .destructive) { action in
            self.setEditing(true, animated: true)
            self.navigationItem.rightBarButtonItem = self.doneBtn  // assign button
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            // handle cancel response here. Doing nothing will dismiss the view.
        }
        
        alert.addAction(addRestaurantAction)
        if self.hasRestaurants {
            alert.addAction(editRestaurantAction)
            alert.addAction(deleteRestaurantAction)
        }
        alert.addAction(cancelAction)
        
        alert.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem //For iPad popover issue
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func endTableEditMode(_ sender: UIBarButtonItem) {
        self.setEditing(false, animated: true)
        setBackground()
        self.navigationItem.rightBarButtonItem = self.editOptionsBtn //restore the add button after editing
    }
    
    @objc func editRestaurant(_ sender: UIButton) {
        if let objs = controller.fetchedObjects, objs.count > 0 {
            
            let restaurant = objs[sender.tag]
            performSegue(withIdentifier: "Add Restaurant", sender: restaurant)
        }
    }
    
    @objc func endRestaurantEdit(_button: UIBarButtonItem!) {
        self.restaurantEditing = false
        self.setEditing(false, animated: true)
        self.navigationItem.rightBarButtonItem = self.editOptionsBtn
        tableView.reloadData()
    }
    
    func setBackground() {
        if let objs = controller.fetchedObjects, objs.count > 0 {
            self.tableView.backgroundView = nil
            self.hasRestaurants = true
        } else {
            self.tableView.backgroundView = noDataView
            self.hasRestaurants = false
        }
    }
    
    func setDeletedBackground(msg: String) {
        self.deleteAllLabel.text = msg
        if let objs = controller.fetchedObjects, objs.count > 0 {
            self.tableView.backgroundView = nil
            self.hasRestaurants = true
        } else {
            self.tableView.backgroundView = deletedAll
            self.hasRestaurants = false
        }
    }

}
