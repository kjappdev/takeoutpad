//
//  AddRestaurantVC.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 2/21/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation

class AddRestaurantVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIScrollViewDelegate  {
    
    @IBOutlet weak var restaurantField: TpCustomTextField!
    @IBOutlet weak var restaurantImage: UIImageView!
    @IBOutlet weak var photoLbl: UILabel!
    @IBOutlet weak var scrollContainer: UIScrollView!
    
    var restaurantToEdit: Restaurant!
    var restaurantEditMode: Bool!
    var imagePicker: UIImagePickerController!
    var doneBtn: UIBarButtonItem!
    var ids: [Int32] = []
    var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.restaurantField.delegate = self
        self.scrollContainer.delegate = self
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.center = view.center
        self.activityIndicator.color = UIColor.gray
        

        // Do any additional setup after loading the view.
        //setBackground()
        //self.navigationItem.title = "New Restaurant"
        
        // Register to be notified if the keyboard is changing size
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShowOrHide), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShowOrHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //set UIbar buttons
        doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(endEdit(_button:)))
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(addRestaurantImage))
        photoLbl.isUserInteractionEnabled = true
        photoLbl.addGestureRecognizer(tap)
        
        self.restaurantImage.layer.cornerRadius = 5.0
        
        if restaurantEditMode {
            self.photoLbl.text = "Edit Photo"
            self.restaurantField.text = restaurantToEdit.name
            self.restaurantImage.image = restaurantToEdit?.toRestaurantImage?.image as? UIImage
            if let currentImage =  restaurantToEdit?.toRestaurantImage?.image as? UIImage {
                restaurantImage.image = currentImage
            } else {
                restaurantImage.image = UIImage(named: "restaurant")
            }
            self.navigationItem.rightBarButtonItem = doneBtn
        } else {
            self.photoLbl.text = "Add Photo"
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
    }
    
    deinit {
        // Don't have to do this on iOS 9+, but it still works
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShowOrHide(notification: NSNotification) {
        
        // Pull a bunch of info out of the notification
        if let scrollView = scrollContainer, let userInfo = notification.userInfo, let endValue = userInfo[UIKeyboardFrameEndUserInfoKey], let durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey] {
            
            // Transform the keyboard's frame into our view's coordinate system
            let endRect = view.convert((endValue as AnyObject).cgRectValue, from: view.window)
            
            // Find out how much the keyboard overlaps the scroll view
            // We can do this because our scroll view's frame is already in our view's coordinate system
            let keyboardOverlap = scrollView.frame.maxY - endRect.origin.y
            
            // Set the scroll view's content inset to avoid the keyboard
            // Don't forget the scroll indicator too!
            scrollView.contentInset.bottom = keyboardOverlap
            scrollView.scrollIndicatorInsets.bottom = keyboardOverlap
            
            let duration = (durationValue as AnyObject).doubleValue
            UIView.animate(withDuration: duration!, delay: 0, options: .beginFromCurrentState, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage {
            restaurantImage.image = resizeImage(image: img, newWidth: 300)
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func saveRestaurant(_ sender: UIBarButtonItem) {
        
        if isValidRestuarantName() {
            saveRestaurantData()
            self.view.endEditing(true)
            self.dismiss(animated: true, completion: nil)
        } else {
            let noNameAlert = UIAlertController(title: nil, message: "Please enter a restaurant name to save.", preferredStyle: .alert)
            
            let okNoNameAction = UIAlertAction(title: "Ok", style: .default) { action in
                // handle response here. Doing nothing will dismiss the view.
            }
            noNameAlert.addAction(okNoNameAction)
            self.present(noNameAlert, animated: true, completion: nil)
        }
    }
    
    @IBAction func addRestaurantImage(_ sender: UIButton) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let addPhotoAction = UIAlertAction(title: "Choose a photo", style: .default) { action in
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.imagePicker = UIImagePickerController()
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: {
                    self.activityIndicator.stopAnimating()
                })
            } else {
                // show failure alert
                let photoLibraryAlert = UIAlertController(title: nil, message: "Your device is not able to access the photo library.", preferredStyle: .alert)
                
                let okPhotoLibraryAction = UIAlertAction(title: "Ok", style: .default) { action in
                    // handle response here. Doing nothing will dismiss the view.
                }
                photoLibraryAlert.addAction(okPhotoLibraryAction)
                self.present(photoLibraryAlert, animated: true, completion: nil)
                self.activityIndicator.stopAnimating()
            }

        }
        
        let takePhotoAction = UIAlertAction(title: "Take a photo", style: .default) { action in
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                let authStatus = AVCaptureDevice.authorizationStatus(for: .video)
                
                switch authStatus {
                case .authorized: self.presentCamera()
                case .notDetermined: self.requestCameraPermission()
                case .restricted, .denied: self.alertCameraAccessNeeded()
                }
                
            } else {
                // show failure alert
                let cameraAlert = UIAlertController(title: nil, message: "Your device is not able to access a camera.", preferredStyle: .alert)
                
                let okCameraAction = UIAlertAction(title: "Ok", style: .default) { action in
                    // handle response here. Doing nothing will dismiss the view.
                }
                cameraAlert.addAction(okCameraAction)
                self.present(cameraAlert, animated: true, completion: nil)
                self.activityIndicator.stopAnimating()
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            // handle cancel response here. Doing nothing will dismiss the view.
        }
        
        alert.addAction(addPhotoAction)
        alert.addAction(takePhotoAction)
        alert.addAction(cancelAction)
        
        self.view.endEditing(true)
        alert.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem //For iPad popover issue
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func cancelAdd(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func endEdit(_button: UIBarButtonItem) {
        if isValidRestuarantName() {
            saveRestaurantData()
            self.view.endEditing(true)
            self.dismiss(animated: true, completion: nil)
        } else {
            let noNameAlert = UIAlertController(title: nil, message: "Please enter a restaurant name to save.", preferredStyle: .alert)
            
            let okNoNameAction = UIAlertAction(title: "Ok", style: .default) { action in
                // handle response here. Doing nothing will dismiss the view.
            }
            noNameAlert.addAction(okNoNameAction)
            self.present(noNameAlert, animated: true, completion: nil)
        }
    }
    
    func saveRestaurantData() {
        var restaurant: Restaurant!
        let strRestaurantName = restaurantField.text
        let trimmedRestaurantName = strRestaurantName?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if restaurantToEdit == nil {
            restaurant = Restaurant(context: context)
            setId(restaurant: restaurant)
        } else {
            restaurant = restaurantToEdit
        }
        
        if let restaurantField = trimmedRestaurantName {
            restaurant.name = restaurantField
        }
        //print(restaurant.name!)
        //print(restaurant.unique)
        
        let picture = RestaurantImage(context: context)
        picture.image = restaurantImage.image
        
        restaurant.toRestaurantImage = picture
        
        ad.saveContext()
    }
    
    func isValidRestuarantName() -> Bool {
        let strRestaurantName = restaurantField.text
        let trimmedRestaurantName = strRestaurantName?.trimmingCharacters(in: .whitespacesAndNewlines)
        if (trimmedRestaurantName?.isEmpty)! {
            return false
        }
        return true
    }
    
    func setBackground() {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "Paper_bkg")
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        self.view.insertSubview(backgroundImage, at: 0)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func setId(restaurant: Restaurant) {
        let fetchRequest: NSFetchRequest<Restaurant> = Restaurant.fetchRequest()
        do {
            let results = try context.fetch(fetchRequest)
            
            for result in results {
                self.ids += [result.unique]
            }
            var id = 0
            for _ in 0..<results.count {
                if self.ids.contains(Int32(id)) {
                    id += 1
                    continue
                } else {
                    restaurant.unique = Int32(id)
                    break
                }
            }
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
        
        
    }
    
    func presentCamera() {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .camera
        self.imagePicker.allowsEditing = false
        self.present(self.imagePicker, animated: true, completion: {
            self.activityIndicator.stopAnimating()
        })
    }
    
    func presentPhotoLibrary() {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = false
        self.present(self.imagePicker, animated: true, completion: {
            self.activityIndicator.stopAnimating()
        })
    }
    
    func requestCameraPermission() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
            self.presentCamera()
        })
    }
    
    func requestPhotoLibraryPermission() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            guard accessGranted == true else { return }
            self.presentPhotoLibrary()
        })
    }
    
    func alertCameraAccessNeeded() {
        let settingsAppURL = URL(string: UIApplicationOpenSettingsURLString)!
        
        let alert = UIAlertController(
            title: "Cannot access camera",
            message: "Please turn on Camera in Settings for TakeoutPad",
            preferredStyle: UIAlertControllerStyle.alert
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true, completion: {
            self.activityIndicator.stopAnimating()
        })
    }
    
    func alertPhotoLibraryAccessNeeded() {
        let settingsAppURL = URL(string: UIApplicationOpenSettingsURLString)!
        
        let alert = UIAlertController(
            title: "Cannot access photos",
            message: "Please turn on Photos in Settings for TakeoutPad",
            preferredStyle: UIAlertControllerStyle.alert
        )
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
        }))
        
        present(alert, animated: true, completion: {
            self.activityIndicator.stopAnimating()
        })
    }

}
