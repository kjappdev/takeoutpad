//
//  Restaurant+CoreDataProperties.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 2/20/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//
//

import Foundation
import CoreData


extension Restaurant {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Restaurant> {
        return NSFetchRequest<Restaurant>(entityName: "Restaurant")
    }

    @NSManaged public var name: String?
    @NSManaged public var unique: Int32
    @NSManaged public var toCustomer: NSSet?
    @NSManaged public var toRestaurantImage: RestaurantImage?

}

// MARK: Generated accessors for toCustomer
extension Restaurant {

    @objc(addToCustomerObject:)
    @NSManaged public func addToToCustomer(_ value: Customer)

    @objc(removeToCustomerObject:)
    @NSManaged public func removeFromToCustomer(_ value: Customer)

    @objc(addToCustomer:)
    @NSManaged public func addToToCustomer(_ values: NSSet)

    @objc(removeToCustomer:)
    @NSManaged public func removeFromToCustomer(_ values: NSSet)

}
