//
//  Order+CoreDataProperties.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 2/20/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//
//

import Foundation
import CoreData


extension Order {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Order> {
        return NSFetchRequest<Order>(entityName: "Order")
    }

    @NSManaged public var details: String?
    @NSManaged public var name: String?
    @NSManaged public var type: String?
    @NSManaged public var toCustomer: Customer?

}
