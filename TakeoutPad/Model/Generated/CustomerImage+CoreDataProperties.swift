//
//  CustomerImage+CoreDataProperties.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 2/20/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//
//

import Foundation
import CoreData


extension CustomerImage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CustomerImage> {
        return NSFetchRequest<CustomerImage>(entityName: "CustomerImage")
    }

    @NSManaged public var image: NSObject?
    @NSManaged public var toCustomer: Customer?

}
