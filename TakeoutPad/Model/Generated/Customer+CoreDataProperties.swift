//
//  Customer+CoreDataProperties.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 2/20/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//
//

import Foundation
import CoreData


extension Customer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Customer> {
        return NSFetchRequest<Customer>(entityName: "Customer")
    }

    @NSManaged public var name: String?
    @NSManaged public var order: String?
    @NSManaged public var unique: Int32
    @NSManaged public var toCustomerImage: CustomerImage?
    @NSManaged public var toOrder: Order?
    @NSManaged public var whichRestaurant: Restaurant?

}
