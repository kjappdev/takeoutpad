//
//  TextFieldPadding.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 2/28/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//
import UIKit

@IBDesignable
class TextFieldPadding: UITextField {

    // Placeholder text
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 4, dy: 0)
    }
    
    // Editable text
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 4, dy: 0)
    }
    
}
