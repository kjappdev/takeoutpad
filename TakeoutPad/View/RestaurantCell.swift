//
//  RestaurantCell.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 2/16/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import Foundation
import UIKit

class RestaurantCell: UITableViewCell {
    
    @IBOutlet weak var title: LabelPadding!
    
    @IBOutlet weak var thumb: UIImageView!
    
    func configureCell(restaurant: Restaurant, editMode: Bool!) {
        title.text = restaurant.name
        thumb.contentMode = UIViewContentMode.scaleAspectFill
        thumb.clipsToBounds = true
        thumb.layer.cornerRadius = 5.0
        thumb.image = UIImage(named: "restaurant")
        if let newImage =  restaurant.toRestaurantImage?.image as? UIImage {
            thumb.image = newImage
        }
        //self.backgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1.0)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectedBackgroundView = UIView()
        self.selectionStyle = .default
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectedBackgroundView!.backgroundColor = UIColor(red: 11/255.0, green: 72/255.0, blue: 37/255.0, alpha: 0.2)
    }
    
}
