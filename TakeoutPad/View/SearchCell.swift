//
//  SearchCell.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 3/9/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    
    @IBOutlet weak var restaurantTitle: UILabel!
    
    @IBOutlet weak var customerTitle: UILabel!
    
    func configureCell(customer: Customer) {
        restaurantTitle.text = customer.whichRestaurant?.name
        customerTitle.text = customer.name
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectedBackgroundView = UIView()
        self.selectionStyle = .default
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectedBackgroundView!.backgroundColor = UIColor(red: 11/255.0, green: 72/255.0, blue: 37/255.0, alpha: 0.2)
    }

}
