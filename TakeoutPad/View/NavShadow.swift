//
//  NavShadow.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 2/18/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import Foundation
import UIKit
extension UINavigationBar {
    
    @IBInspectable var shadow: Bool {
        get {
            return false
        }
        set {
            if newValue {
                self.layer.shadowOffset = CGSize(width: 0, height: 2)
                self.layer.shadowColor = UIColor.black.cgColor
                self.layer.shadowRadius = 3
                self.layer.shadowOpacity = 0.5;
            }
        }
    }
}
