//
//  MoreCell.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 3/28/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import UIKit

class MoreCell: UITableViewCell {

    @IBOutlet weak var shareThumb: UIImageView!
    
    @IBOutlet weak var shareTitle: LabelPadding!
    
    func configureCell(shareType: String) {
        shareThumb.contentMode = UIViewContentMode.scaleAspectFill
        shareThumb.clipsToBounds = true
        shareThumb.layer.cornerRadius = 5.0
        
        switch shareType {
        case "support":
            shareTitle.text = "Feedback"
            shareThumb.image = UIImage(named: "mail-icon")
            break
        default:
            shareTitle.text = "Feedback"
            shareThumb.image = UIImage(named: "mail-icon")
            break
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectedBackgroundView = UIView()
        self.selectionStyle = .default
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectedBackgroundView!.backgroundColor = UIColor(red: 11/255.0, green: 72/255.0, blue: 37/255.0, alpha: 0.2)
    }
    
}
