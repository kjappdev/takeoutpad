//
//  CustomProvider.swift
//  TakeoutPad
//
//  Created by Kevin Jefferson on 3/20/18.
//  Copyright © 2018 Kevin Jefferson. All rights reserved.
//

import UIKit

class CustomProvider : UIActivityItemProvider {
    
    var facebookSubject: String!
    var twitterSubject: String!
    var emailSubject: String!
    var textMessageSubject: String!
    var facebookMessage : String!
    var twitterMessage : String!
    var emailMessage : String!
    var textMessage: String!
    
    override func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    
    override func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivityType?) -> String {
        switch activityType {
        case UIActivityType.postToFacebook?:
            return self.facebookSubject
        case UIActivityType.postToTwitter?:
            return self.twitterSubject
        case UIActivityType.mail?:
            return self.emailSubject
        case UIActivityType.message?:
            return self.textMessageSubject
        default:
            return ""
        }
    }
    
    override func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType?) -> Any? {
        switch activityType {
        case UIActivityType.postToFacebook?:
            return self.facebookMessage
        case UIActivityType.postToTwitter?:
            return self.twitterMessage
        case UIActivityType.mail?:
            return self.emailMessage
        case UIActivityType.message?:
            return self.textMessage
        default:
            return nil
        }
    }
    
}
